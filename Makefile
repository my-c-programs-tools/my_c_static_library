##
## Makefile for libmy in /home/verove-j/EtnaRendu/libmy/tmp
## 
## Made by VEROVE Jordan
## Login   <verove_j@etna-alternance.net>
## 
## Started on  Sat Oct 22 16:23:15 2016 VEROVE Jordan
## Last update Sun May  3 21:03:38 2020 VEROVE Jordan
##

SRCS_DIR =	srcs/

INCLUDES =	-Iincludes -I.

CC =		gcc

CFLAGS =	$(INCLUDES) -Wall -Werror -Wextra

NAME =		libmy.a

SRC =		$(SRCS_DIR)my_putchar.c \
		$(SRCS_DIR)my_putstr.c \
		$(SRCS_DIR)my_isneg.c \
		$(SRCS_DIR)my_put_nbr.c \
		$(SRCS_DIR)my_swap.c \
		$(SRCS_DIR)my_strlen.c \
		$(SRCS_DIR)my_getnbr.c \
		$(SRCS_DIR)my_strcpy.c \
		$(SRCS_DIR)my_strncpy.c \
		$(SRCS_DIR)my_strcmp.c \
		$(SRCS_DIR)my_strncmp.c \
		$(SRCS_DIR)my_strcat.c \
		$(SRCS_DIR)my_strncat.c \
		$(SRCS_DIR)my_strstr.c \
		$(SRCS_DIR)my_strdup.c \
		$(SRCS_DIR)my_str_to_wordtab.c \
		$(SRCS_DIR)my_add_list_to_list.c \
		$(SRCS_DIR)my_add_sort_list_to_sort_list.c \
		$(SRCS_DIR)my_find_elem_eq_in_list.c \
		$(SRCS_DIR)my_find_node_elem_eq_in_list.c \
		$(SRCS_DIR)my_put_elem_in_sort_list.c \
		$(SRCS_DIR)my_rm_all_eq_on_list.c

OBJ =		$(SRC:.c=.o)

RM =		rm -rf

$(NAME):	$(OBJ)
		ar -rc $(NAME) $(OBJ)
		ranlib $(NAME)

all:		$(NAME)

clean:
		$(RM) $(OBJ)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		all clean fclean re
