/*
** my_isneg.c for my_isneg in /home/verove-j/EtnaRendu/Jour01-C/verove_j/my_isneg
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Mon Oct 17 14:47:12 2016 VEROVE Jordan
** Last update Mon Oct 17 16:51:23 2016 VEROVE Jordan
*/

int	my_isneg(int i)
{
  if (i < 0)
    return (0);
  return (1);
}
