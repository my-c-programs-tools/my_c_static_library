/*
** my_strdup.c for my_strdup in /home/jordan/rendu/Jour07-C/verove_j/my_strdup
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Mon Oct 24 11:42:41 2016 VEROVE Jordan
** Last update Mon Oct 24 15:55:21 2016 VEROVE Jordan
*/

#include <stdlib.h>

int	my_strlen(char *str);

char	*my_strdup(char *str)
{
  int	i;
  int	str_size;
  char	*new_str;

  if ((str_size = my_strlen(str)) != 0)
    {
      if ((new_str = malloc(sizeof(char) * (str_size + 1))) == NULL)
	return (NULL);
      i = 0;
      while (*str)
	new_str[i++] = *str++;
    }
  return (new_str);
}
