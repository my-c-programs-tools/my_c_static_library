/*
** my_strncmp.c for my_strncmp in /home/jordan/rendu/Jour04-C/verove_j/my_strncmp
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 21 12:03:02 2016 VEROVE Jordan
** Last update Sat Oct 22 18:37:37 2016 VEROVE Jordan
*/

int	my_strncmp(char *s1, char *s2, int nb)
{
  int	i;

  i = 0;
  while (i < (nb - 1) && s1[i] && s2[i] && (s1[i] == s2[i]))
    i++;
  if ((s1[i] - s2[i]) > 0)
    return (1);
  else if ((s1[i] - s2[i]) < 0)
    return (-1);
  else
    return (0);
}
