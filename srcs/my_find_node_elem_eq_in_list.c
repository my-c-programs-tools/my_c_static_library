/*
** my_find_node_elm_eq_in_list.c for my_find_node_eq_in_list in /home/jordan/rendu/Jour10-C/tmp/my_find_node_elm_eq_in_list
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Thu Oct 27 11:24:11 2016 VEROVE Jordan
** Last update Wed Oct  9 10:26:29 2019 VEROVE Jordan
*/

#include <stdlib.h>
#include "my_list.h"

t_list		*my_find_node_elm_eq_in_list(t_list *begin, void *data_ref,
					     int (*cmp)())
{
    t_list	*tmp;

  tmp = begin;
  while (tmp != NULL)
    {
      if (cmp(tmp->data, data_ref) == 0)
        return (tmp);
      tmp = tmp->next;
    }
  return (NULL);
}
