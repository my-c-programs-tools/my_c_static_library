/*
** my_strcmp.c for my_strcmp in /home/jordan/rendu/Jour04-C/verove_j/my_strcmp
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 21 11:42:27 2016 VEROVE Jordan
** Last update Thu Oct 17 11:29:39 2019 VEROVE Jordan
*/

int	my_strlen(char *str);

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  if (my_strlen(s1) < my_strlen(s2))
    return (-1);
  else if (my_strlen(s1) > my_strlen(s2))
    return (1);
  while (s1[i] != 0)
    {
      if (s1[i] < s2[i])
	return (-1);
      else if (s1[i] > s2[i])
	return (1);
      i++;
    }
  return (0);
}
