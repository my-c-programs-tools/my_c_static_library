/*
** my_put_elem_in_sort_list.c for my_put_elem_in_sort_list in /home/jordan/rendu/Jour10-C/tmp/my_put_elem_in_sort_list
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Thu Oct 27 18:21:00 2016 VEROVE Jordan
** Last update Wed Oct  9 10:32:21 2019 VEROVE Jordan
*/

#include <stdlib.h>
#include "my_list.h"

void		my_swap_elem_in_list(t_list **begin, t_list *first,
				      t_list *second, t_list *prev)
{
  t_list	*tmp;

  tmp = first;
  first->next = second->next;
  second->next = tmp;
  if (prev == NULL)
    *begin = second;
  else
    prev->next = second;
}

void		my_sort_list(t_list **begin, int (*cmp)())
{
  t_list	*prev;
  t_list	*tmp;
  int		check;

  tmp = *begin;
  prev = NULL;
  check = 1;
  while (check != 0)
    {
      check = 0;
      while (tmp != NULL && tmp->next != NULL)
	{
	  if (cmp(tmp->data, (tmp->next)->data) > 0)
	    {
	      my_swap_elem_in_list(begin, tmp, tmp->next, prev);
	      check += 1;
	    }
	  prev = tmp;
	  tmp = tmp->next;
	}
      tmp = *begin;
      prev = NULL;
    }
}

void		my_put_elem_in_sort_list(t_list **begin,
					 void *data, int (*cmp)())
{
  t_list	*new_elem;

  if (!((new_elem = malloc(sizeof(t_list))) == NULL))
    {
      new_elem->data = data;
      new_elem->next = *begin;
      *begin = new_elem;
      my_sort_list(begin, cmp);
    }
}
