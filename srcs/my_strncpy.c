/*
** my_strncpy.c for my_strncpy in /home/jordan/rendu/Jour04-C/my_strncpy
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 21 11:04:21 2016 VEROVE Jordan
** Last update Sat Oct 22 19:47:49 2016 VEROVE Jordan
*/

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;

  i = 0;
  while (i < n && src[i] != '\0')
    {
      dest[i] = src[i];
      i++;
    }
  while (i < n)
    {
      dest[i] = '\0';
      i++;
    }
  return (dest);
}
