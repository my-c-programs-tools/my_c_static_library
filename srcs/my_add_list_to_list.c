/*
** my_add_list_to_list.c for my_add_list_to_list in /home/jordan/rendu/Jour10-C/tmp/my_add_list_to_list
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Thu Oct 27 13:46:02 2016 VEROVE Jordan
** Last update Thu Oct 27 14:01:40 2016 VEROVE Jordan
*/

#include <stdlib.h>
#include "my_list.h"

void		my_add_list_to_list(t_list **begin1, t_list *begin2)
{
  t_list	*cur;
  t_list	*tmp;

  cur = begin2;
  tmp = begin2;
  while (cur->next != NULL)
    cur = cur->next;
  cur->next = *begin1;
  *begin1 = tmp;
}
