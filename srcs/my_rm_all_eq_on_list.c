/*
** my_rm_all_eq_on_list.c for my_rm_all_eq_from_list in /home/jordan/rendu/Jour10-C/tmp/my_rm_all_eq_from_list
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Thu Oct 27 11:30:57 2016 VEROVE Jordan
** Last update Thu Oct 27 14:04:20 2016 VEROVE Jordan
*/

#include <stdlib.h>
#include "my_list.h"

t_list		*my_rm_in_list(t_list **begin, t_list *cur, t_list *prev)
{
  t_list	*tmp;

  tmp = cur->next;
  if (prev == NULL)
    *begin = cur->next;
  else
    prev->next = cur->next;
  free(cur);
  return (tmp);
}

void		my_rm_all_eq_from_list(t_list **begin, void *data_ref,
			       int (*cmp)())
{
  t_list	*cur;
  t_list	*prev;

  cur = *begin;
  prev = NULL;
  while (cur->next != NULL)
    {
      if (cmp(cur->data, data_ref) == 0)
	cur = my_rm_in_list(begin, cur, prev);
      else
	{
	  prev = cur;
	  cur = cur->next;
	}
    }
  if (cmp(cur->data, data_ref) == 0)
    free(cur);
}
