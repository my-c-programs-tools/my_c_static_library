/*
** my_strcat.c for my_strcat in /home/jordan/rendu/Jour04-C/verove_j/my_strcat
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 21 12:09:53 2016 VEROVE Jordan
** Last update Fri Oct 21 12:18:18 2016 VEROVE Jordan
*/

char	*my_strcat(char *dest, char *src)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (dest[i] != 0)
    i++;
  if (dest[i] == 0)
    {
      while (src[j] != 0)
	dest[i++] = src[j++];
      dest[i] = 0;
    }
  return (dest);
}
