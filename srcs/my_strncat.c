/*
** my_strncat.c for my_strncat in /home/jordan/rendu/Jour04-C/verove_j/my_strncat
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 21 12:17:49 2016 VEROVE Jordan
** Last update Fri Oct 21 12:25:29 2016 VEROVE Jordan
*/

char	*my_strncat(char *dest, char *src, int n)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (dest[i] != 0)
    i++;
  if (dest[i] == 0)
    {
      while (j != n && src[j] != 0)
	dest[i++] = src[j++];
      dest[i] = 0;
    }
  return (dest);
}
