/*
** my_add_sort_list_to_sort_list.c for my_add_sort_list_to_sort_list in /home/jordan/rendu/Jour10-C/tmp/my_add_sort_list_to_sort_list
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Thu Oct 27 18:25:48 2016 VEROVE Jordan
** Last update Wed Oct  9 10:22:40 2019 VEROVE Jordan
*/

#include <stdlib.h>
#include "my_list.h"

void		my_add_list_to_list(t_list **begin1, t_list *begin2)
{
  t_list	*cur;
  t_list	*tmp;

  cur = begin2;
  tmp = begin2;
  while (cur->next != NULL)
    cur = cur->next;
  cur->next = *begin1;
  *begin1 = tmp;
}

void		my_swap_elem_in_list(t_list **begin, t_list *first,
				      t_list *second, t_list *prev)
{
  t_list	*tmp;

  tmp = first;
  first->next = second->next;
  second->next = tmp;
  if (prev == NULL)
    *begin = second;
  else
    prev->next = second;
}

void		my_sort_list(t_list **begin, int (*cmp)())
{
  t_list	*prev;
  t_list	*tmp;
  int		check;

  tmp = *begin;
  prev = NULL;
  check = 1;
  while (check != 0)
    {
      check = 0;
      while (tmp != NULL && tmp->next != NULL)
	{
	  if (cmp(tmp->data, (tmp->next)->data) > 0)
	    {
	      my_swap_elem_in_list(begin, tmp, tmp->next, prev);
	      check += 1;
	    }
	  prev = tmp;
	  tmp = tmp->next;
	}
      tmp = *begin;
      prev = NULL;
    }
}

void	my_add_sort_list_to_sort_list(t_list **begin1,
				      t_list *begin2, int (*cmp)())
{
  my_add_list_to_list(begin1, begin2);
  my_sort_list(begin1, cmp);
}
