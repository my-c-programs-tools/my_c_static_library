/*
** my_getnbr.c for libmy in /home/verove-j/EtnaRendu/libmy/tmp
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Sat Oct 22 15:36:58 2016 VEROVE Jordan
** Last update Thu Oct 17 14:48:01 2019 VEROVE Jordan
*/

int	my_getnbr(char *str)
{
  int	nb;
  int	sign;

  sign = 1;
  nb = 0;
  while (*str && (*str == '-' || *str == '+'))
    sign *= (*(str++) == '+') ? (1) : (-1);
  while (*str && *str >= '0' && *str <= '9')
    nb = (nb * 10) + (*(str++) - '0');
  return (nb * sign);
}
