/*
** my_strlen.c for my_strlen in /home/verove-j/EtnaRendu/Jour03-C/my_strlen
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Thu Oct 20 10:07:46 2016 VEROVE Jordan
** Last update Thu Oct 20 10:09:45 2016 VEROVE Jordan
*/

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != 0)
    i++;
  return (i);
}
