/*
** my_str_to_wordtab.c for my_str_to_wordtab in /home/jordan/rendu/Jour07-C/verove_j/my_str_to_wordtab
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Mon Oct 24 12:03:50 2016 VEROVE Jordan
** Last update Mon Oct 24 14:19:00 2016 VEROVE Jordan
*/

#include <stdlib.h>

int	my_word_len(char *str)
{
  int	i;

  i = 0;
  while ((str[i] <= '9' && str[i] >= '0') || (str[i] <= 'z' && str[i] >= 'a')
	 || (str[i] <= 'Z' && str[i] >= 'A'))
    i++;
  return (i);
}

int	my_wordtab_len(char *str)
{
  int	len;

  len = 0;
  while (*str)
    {
      if ((*str <= '9' && *str >= '0') || (*str <= 'z' && *str >= 'a')
	     || (*str <= 'Z' && *str >= 'A'))
	{
	  len += 1;
	  while ((*str <= '9' && *str >= '0') || (*str <= 'z' && *str >= 'a')
		 || (*str <= 'Z' && *str >= 'A'))
	    str++;
	}
      if (*str)
	str++;
    }
  return (len);
}

char	*cpy_in_tab(char *str)
{
  char	*word;
  int	i;

  i = 0;
  if ((word = malloc((my_word_len(str) + 1) * sizeof(char))) == NULL)
    return (NULL);
  while ((*str <= '9' && *str >= '0') || (*str <= 'z' && *str >= 'a')
	     || (*str <= 'Z' && *str >= 'A'))
    {
      word[i++] = *str;
      str++;
    }
  return (word);
}

char	**my_str_to_wordtab(char *str)
{
  char	**wordtab;
  int	wordtab_len;
  int	i;

  i = 0;
  wordtab_len = my_wordtab_len(str);
  if ((wordtab = malloc((wordtab_len + 1) * sizeof(char *))) == NULL)
    return (NULL);
  while (i < wordtab_len)
    {
      if ((*str <= '9' && *str >= '0') || (*str <= 'z' && *str >= 'a')
	     || (*str <= 'Z' && *str >= 'A'))
	{
	  wordtab[i] = cpy_in_tab(str);
	  while ((*str <= '9' && *str >= '0') || (*str <= 'z' && *str >= 'a')
	     || (*str <= 'Z' && *str >= 'A'))
	    str++;
	  i++;
	}
      if (*str)
	str++;
    }
  wordtab[i] = NULL;
  return (wordtab);
}
