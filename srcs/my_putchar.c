/*
** my_putchar.c for libmy_01 in /home/verove-j/EtnaRendu/libmy/tmp
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Sat Oct 22 15:02:38 2016 VEROVE Jordan
** Last update Sat Oct 22 15:03:14 2016 VEROVE Jordan
*/

#include <unistd.h>

void	my_putchar(char c)
{
  write(1, &c, 1);
}
