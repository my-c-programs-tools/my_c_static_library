/*
** my_strcpy.c for my_strcpy in /home/jordan/rendu/Jour04-C/my_strcpy
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 21 11:02:40 2016 VEROVE Jordan
** Last update Fri Oct 21 11:03:44 2016 VEROVE Jordan
*/

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i] != 0)
    {
      dest[i] = src[i];
      i++;
    }
  dest[i] = 0;
  return (dest);
}
