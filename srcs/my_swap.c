/*
** my_swap.c for my_swap in /home/verove-j/EtnaRendu/Jour03-C/verove_j/my_swap
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Thu Oct 20 10:41:47 2016 VEROVE Jordan
** Last update Thu Oct 20 10:59:55 2016 VEROVE Jordan
*/

void	my_swap(int *a, int *b)
{
  int	tmp;

  tmp = *a;
  *a = *b;
  *b = tmp;
}
