/*
** my_putstr.c for my_putstr in /home/verove-j/EtnaRendu/Jour03-C/my_putstr
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Thu Oct 20 09:29:29 2016 VEROVE Jordan
** Last update Sat Oct 22 16:47:37 2016 VEROVE Jordan
*/

void	my_putchar(char c);

void	my_putstr(char *str)
{
  while (*str != 0)
    my_putchar(*str++);
}
