/*
** my_strstr.c for my_strstr in /home/jordan/rendu/Jour04-C/verove_j/my_strstr
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 21 13:30:49 2016 VEROVE Jordan
** Last update Sat Oct 22 16:51:25 2016 VEROVE Jordan
*/

#include <stdlib.h>

int	my_strncmp(char *s1, char *s2, int nb);
int	my_strlen(char *str);

char	*my_strstr(char *str, char *to_find)
{
  int	i;
  int	to_find_size;

  i = 0;
  to_find_size = my_strlen(to_find);
  while (str[i] != 0)
    {
      if (my_strncmp(&str[i], to_find, to_find_size) == 0)
	return (&str[i]);
      i++;
    }
  return (NULL);
}
